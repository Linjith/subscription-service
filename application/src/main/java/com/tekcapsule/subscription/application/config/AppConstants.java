package com.tekcapsule.subscription.application.config;

public final class AppConstants {

    private AppConstants() {

    }

    public static final String HTTP_STATUS_CODE_HEADER = "statuscode";
}
